package android.example.activityscore.weight

import android.content.Intent
import android.example.activityscore.R
import android.example.activityscore.settings.Settings
import android.graphics.Bitmap
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.util.*


class Weight : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bmi)

        setSupportActionBar(findViewById(R.id.my_toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    fun setViews(view : View) {
        val currentWeight = findViewById<EditText>(R.id.etBmiWeight)
        val currentHeight = findViewById<EditText>(R.id.etBmiHeight)

        val textViewBMI = findViewById<TextView>(R.id.tvBmiTitle)
        val textViewBody = findViewById<TextView>(R.id.tvBmiBody)
        textViewBMI.text = ""
        textViewBody.text = ""

        val strWeight = currentWeight.text.toString()
        val strHeight = currentHeight.text.toString()
        if(strHeight.isNotEmpty()&& strWeight.isNotEmpty()){
            val weight = java.lang.Double.valueOf(strWeight)
            var height = java.lang.Double.valueOf(strHeight)

            if(height != 0.0 && weight != 0.0){
                height /= 100
                val bmi = weight / (height * height)

                val range = generateBmiRangeText(bmi)
                val bodyText = String.format(Locale.ENGLISH, getText(R.string.bmi_body).toString(), range)
                val titleText = String.format(Locale.ENGLISH, getText(R.string.bmi_main).toString(), bmi)

                textViewBMI.text = titleText
                textViewBody.text = bodyText
            }
            else{
                textViewBMI.text = "Wrong values entered (0)"
                textViewBody.text = ""
            }
        }
    }

    private fun generateBmiRangeText(bmi : Double): String? {
        if (bmi < 18.5) {
            return getText(R.string.underweight).toString()
        }
        if (bmi < 24.9) {
            return getText(R.string.normal).toString()
        }
        if (bmi < 29.9) {
            return getText(R.string.overweight).toString()
        }
        if (bmi < 34.9) {
            return getText(R.string.obese).toString()
        }
        return if (bmi >= 34.9) {
            getText(R.string.extremely_obese).toString()
        } else ""
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean { // Inflate the menu items for use in the action bar
        menuInflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_settings -> {
            val intent = Intent(this, Settings::class.java)
            startActivity(intent)
            true
        }

        R.id.action_profile -> {
            true
        }

        else -> {
            super.onOptionsItemSelected(item)
        }
    }
}
