package android.example.activityscore.steps_and_running

import android.content.Context
import android.content.Intent
import android.example.activityscore.R
import android.example.activityscore.settings.Settings
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ListAdapter
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity


class StepsRunning : AppCompatActivity(), SensorEventListener {
    private val mSensorListView: ListView? = null
    private val mListAdapter: ListAdapter? = null

    private var mSensorManager: SensorManager? = null
    private var mSensor: Sensor? = null
    private var isSensorPresent = false
    private var mStepsSinceReboot: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_steps_running)
        setSupportActionBar(findViewById(R.id.my_toolbar))

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mStepsSinceReboot = findViewById<View>(R.id.stepsValue) as TextView
        mSensorManager =
            this.getSystemService(Context.SENSOR_SERVICE) as SensorManager

        if (mSensorManager!!.getDefaultSensor(Sensor.TYPE_STEP_COUNTER)
            != null
        ) {
            mSensor = mSensorManager!!.getDefaultSensor(Sensor.TYPE_STEP_COUNTER)
            isSensorPresent = true
        } else {
            isSensorPresent = false
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean { // Inflate the menu items for use in the action bar
        menuInflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_settings -> {
            val intent = Intent(this, Settings::class.java)
            startActivity(intent)
            true
        }

        R.id.action_profile -> {
            true
        }

        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    // what does this do
    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
    }

    override fun onResume() {
        super.onResume()
        if (isSensorPresent) {
            mSensorManager!!.registerListener(
                this, mSensor,
                SensorManager.SENSOR_DELAY_NORMAL
            )
        }
        else {
            Toast.makeText(this, "No Step Counter Sensor !", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onPause() {
        super.onPause()
        if (isSensorPresent) {
            mSensorManager!!.unregisterListener(this)
        }
    }

    override fun onSensorChanged(event: SensorEvent) {
        mStepsSinceReboot!!.text = event.values[0].toString()
    }
}
