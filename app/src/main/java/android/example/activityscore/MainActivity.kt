package android.example.activityscore

import android.content.Intent
import android.example.activityscore.weight.Weight
import android.example.activityscore.fitness.Fitness
import android.example.activityscore.hiit.HIIT
import android.example.activityscore.meal_proposals.MealProposals
import android.example.activityscore.nutrition.Nutrition
import android.example.activityscore.sleep_tracking.SleepTracking
import android.example.activityscore.statistics.Statistics
import android.example.activityscore.steps_and_running.StepsRunning
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /* Verify the action and get the query
        if (Intent.ACTION_SEARCH == intent.action) {
            intent.getStringExtra(SearchManager.QUERY)?.also { query ->
                doMySearch(query)
            }
        }*/
    }

    fun goToNutrition(view : View){
        val intent = Intent(this, Nutrition::class.java)
        startActivity(intent)
    }

    fun goToSteps(view : View){
        val intent = Intent(this, StepsRunning::class.java)
        startActivity(intent)
    }

    fun goToFitness(view : View){
        val intent = Intent(this, Fitness::class.java)
        startActivity(intent)
    }

    fun goToHIIT(view : View){
        val intent = Intent(this, HIIT::class.java)
        startActivity(intent)
    }

    fun goToWeight(view : View){
        val intent = Intent(this, Weight::class.java)
        startActivity(intent)
    }

    fun goToSleepTracking(view : View){
        val intent = Intent(this, SleepTracking::class.java)
        startActivity(intent)
    }

    fun goToStatistics(view : View){
        val intent = Intent(this, Statistics::class.java)
        startActivity(intent)
    }

    fun goToMealProposals(view : View){
        val intent = Intent(this, MealProposals::class.java)
        startActivity(intent)
    }

}


