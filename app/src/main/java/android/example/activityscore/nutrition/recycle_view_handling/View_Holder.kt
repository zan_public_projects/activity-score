package android.example.activityscore

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView.ViewHolder


class View_Holder internal constructor(itemView: View) : ViewHolder(itemView) {
    var cv: CardView
    var title: TextView
    var description: TextView

    init {
        cv = itemView.findViewById(R.id.cardView)
        title = itemView.findViewById(R.id.title)
        description = itemView.findViewById(R.id.description)
    }
}