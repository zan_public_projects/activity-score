package android.example.activityscore.nutrition

import android.content.Intent
import android.example.activityscore.R
import android.example.activityscore.Recycler_View_Adapter
import android.example.activityscore.settings.Settings
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import okhttp3.*
import org.json.JSONArray
import recycle_view_handling.Data
import java.io.IOException


class Nutrition : AppCompatActivity() {
    private val host = "food-calorie-data-search.p.rapidapi.com"
    private val key  = "1f6f5d4dd5msh78fa3a2442bcd97p100ef9jsn7c6682cd4f08"
    private val hostApi = "x-rapidapi-host"
    private val keyApi  = "x-rapidapi-key"
    private val urlAPI  = "https://food-calorie-data-search.p.rapidapi.com/api/search?keyword="
    lateinit var mAdapter : Recycler_View_Adapter
    lateinit var recyclerView : RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nutrition)

        val data: ArrayList<Data>? = fill_with_data()

        recyclerView = findViewById(R.id.recyclerview)
        if(data != null){
            mAdapter = Recycler_View_Adapter(data, application)
            recyclerView.adapter = mAdapter
            recyclerView.layoutManager = LinearLayoutManager(this)
        }

        // my_child_toolbar is defined in the layout file
        setSupportActionBar(findViewById(R.id.my_toolbar))
        // Get a support ActionBar corresponding to this toolbar and enable the Up button
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }


    fun getRequest(food : String?) {
        val client = OkHttpClient()

        if(food != null){
            val procesFood = urlAPI  + food.trim()
            val request = Request.Builder()
                .header(hostApi, host)
                .header(keyApi, key)
                .url(procesFood)
                .build()


            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    e.printStackTrace()
                }

                override fun onResponse(call: Call, response: Response) {
                    response.use {
                        if (!response.isSuccessful) throw IOException("Unexpected code $response")

                        val mArray = JSONArray(response.body!!.string())
                        mAdapter.emptyList()

                        var break_out = 0
                        for (index in 0 until mArray.length()) {
                            val mJsonObject = mArray.getJSONObject(index)

                            val data =  Data(mJsonObject.getString("shrt_desc"), "Calories: " + mJsonObject.getString("energ_kcal") + " kcal," +
                            " Protein: " + mJsonObject.getString("protein") + " g, Carbohydrates: " + mJsonObject.getString("carbohydrt") + " g")
                            mAdapter.insertInList(data)

                            break_out += 1
                            if(break_out >= 20){
                                break
                            }
                        }

                        runOnUiThread {
                            recyclerView.adapter = mAdapter

                        }
                    }
                }
            })
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean { // Inflate the menu items for use in the action bar
        menuInflater.inflate(R.menu.menu, menu)
        //return super.onCreateOptionsMenu(menu)

        val searchView= findViewById<SearchView>(R.id.searchView)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean {
                //Log.e("onQueryTextChange", "called");
                return false
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                // task here
                getRequest(query)
                return false
            }
        })
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_settings -> {
            val intent = Intent(this, Settings::class.java)
            startActivity(intent)
            true
        }

        R.id.action_profile -> {
            // User chose the "Profile" action, mark the current item
            // as a favorite...
            true
        }

        else -> {
            // If we got here, the user's action was not recognized. Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }

    fun fill_with_data(): ArrayList<Data>? {
        val data: ArrayList<Data> = ArrayList()
        return data
    }
}
