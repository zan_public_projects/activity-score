package android.example.activityscore

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import recycle_view_handling.Data
import kotlin.collections.ArrayList


class Recycler_View_Adapter(arrayList: ArrayList<Data>, context: Context) : RecyclerView.Adapter<View_Holder>() {
    var arrayList: ArrayList<Data> = ArrayList()
    var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): View_Holder {
        //Inflate the layout, initialize the View Holder
        val v: View =
            LayoutInflater.from(parent.context).inflate(R.layout.row_layout, parent, false)
        return View_Holder(v)
    }

    override fun onBindViewHolder(holder: View_Holder, position: Int) {
        //Use the provided View Holder on the onCreateViewHolder method to populate the current row on the RecyclerView
        holder.title.setText(arrayList[position].title)
        holder.description.setText(arrayList[position].description)
        //animate(holder);
    }

    override fun getItemCount(): Int { //returns the number of elements the RecyclerView will display
        return arrayList.size
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }


    // Remove a RecyclerView item containing a specified Data object
    fun remove(data: Data?) {
        val position = arrayList.indexOf(data)
        notifyItemRemoved(position)
    }

    fun insertInList(data: Data) {
        arrayList.add(data)
    }

    fun emptyList(){
        arrayList.clear()
    }

    init {
        this.arrayList = arrayList
        this.context = context
    }
}